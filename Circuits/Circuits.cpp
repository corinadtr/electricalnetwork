// Circuits.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>

using namespace std;

const signed char CircuitsNumber = 16;
int circuits[CircuitsNumber][3];

const int VALUE = 0;
const int LAST_UPDATED = 1;
const int PHASE = 2;

const signed char PhasesNumber = 3;
int phasesTotals[PhasesNumber];

const int MaxPhaseAmperage = 20;


int main()
{
	for (int i = 0; i < 5; i++) 
	{
		circuits[i][LAST_UPDATED] = 0;
		circuits[i][PHASE] = 1;
	}
	for (int i = 5; i < 10; i++)
	{
		circuits[i][LAST_UPDATED] = 0;
		circuits[i][PHASE] = 2;
	}
	for (int i = 10; i < 16; i++)
	{
		circuits[i][LAST_UPDATED] = 0;
		circuits[i][PHASE] = 3;
	}
	
	for ( int j = 1; j < 1000; j++ ) 
	{
		clock_t initialTime = clock();

		float diff = 0.02;

		do
		{
			cout << diff << endl;
			cout << clock() << endl;
			diff = (float)(clock() - initialTime) / CLOCKS_PER_SEC;

		} while (diff < 0.02);

		cout << "-----------" << endl;
		for (int i = 0; i < CircuitsNumber; i++)
		{
			circuits[i][VALUE] = rand() % 10;

			cout << circuits[i][VALUE] << circuits[i][LAST_UPDATED] << circuits[i][PHASE] << endl;
		}
	}
}

void switchCircuitOff(int phaseIndex, int phaseTotalValue)
{
	bool phaseOk = false;

	while (!phaseOk)
	{
		for (int i = 0; i < CircuitsNumber; i++)
		{
			if (circuits[i][PHASE] == phaseIndex && phaseTotalValue - circuits[i][VALUE] <= MaxPhaseAmperage && circuits[i][LAST_UPDATED] == 0)
			{
				circuits[i][VALUE] = 0;
				circuits[i][LAST_UPDATED] = 1;
				phaseOk = true;
				break;
			}
		}
	}
}

void analyzeElectricalNetwork(bool valueProcessed)
{
	if (!valueProcessed)
		return;

	for (int i = 0; i < PhasesNumber; i++)
	{
		if(phasesTotals[i] >= MaxPhaseAmperage)
		{
			switchCircuitOff(i, phasesTotals[i]);
		}
	}
}
